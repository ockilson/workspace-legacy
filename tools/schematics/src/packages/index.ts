import { Rule, Tree, SchematicContext, chain } from "@angular-devkit/schematics";
import { addSchematicsFilesToProject } from "@ockilson/schematics-utils";

export function lernaPackage(_options: any): Rule {
    return (tree: Tree, _context: SchematicContext) => {
        return chain([
            addSchematicsFilesToProject(_options, 'packages')
        ]);
    }
}