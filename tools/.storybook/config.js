import { configure, addDecorator } from '@storybook/angular';
import { checkA11y } from '@storybook/addon-a11y';
import { withNotes } from '@storybook/addon-notes';
import { withLinks } from '@storybook/addon-links';

// automatically import all files ending in *.stories.ts
let hasProjects = true;
try {
  const projects = require.context('../../projects', true, /.stories.ts$/);
} catch(e) {
  hasProjects = false;
}
const rootProject = require.context('../../src', true, /.stories.ts$/)

addDecorator(checkA11y);
addDecorator(withNotes);
addDecorator(withLinks);

function loadStories() {
  if(hasProjects) {
    projects.keys().forEach(filename => projects(filename));
  }
  rootProject.keys().forEach(filename => rootProject(filename));
}

configure(loadStories, module);
