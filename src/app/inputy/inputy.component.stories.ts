import { storiesOf } from '@storybook/angular';

import { InputyComponent } from './inputy.component';

const stories = storiesOf('InputyComponent', module);

stories.add('InputyComponent basic story', () => ({
    component: InputyComponent,
    props: {
        content: 'custom story properties'
    }
}));

stories.add('InputyComponent alt basic story', () => ({
    component: InputyComponent,
    props: {
        content: 'THIS IS SPARTA'
    }
}));

stories.add('InputyComponent without input', () => ({
    component: InputyComponent
}));