import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputyComponent } from './inputy.component';

describe('InputyComponent', () => {
  let component: InputyComponent;
  let fixture: ComponentFixture<InputyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
