import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'app-inputy',
  template: `
    <p>
      inputy works with {{content}}
    </p>
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InputyComponent {
  @Input() content: string = 'Default Content';

  constructor() { }

}
