import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoryComponent } from './story/story.component';
import { EventfulComponent } from './eventful/eventful.component';
import { InputyComponent } from './inputy/inputy.component';

@NgModule({
  declarations: [
    AppComponent,
    StoryComponent,
    EventfulComponent,
    InputyComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [StoryComponent, EventfulComponent, InputyComponent]
})
export class AppModule { }
