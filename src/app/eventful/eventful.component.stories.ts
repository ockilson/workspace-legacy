import { storiesOf } from '@storybook/angular';
import { action } from '@storybook/addon-actions';

import { EventfulComponent } from './eventful.component';

const stories = storiesOf('EventfulComponent', module);

stories.add('EventfulComponent basic story', () => ({
    component: EventfulComponent,
    props: {
        thinged: action('Did a thing')
    }
}));