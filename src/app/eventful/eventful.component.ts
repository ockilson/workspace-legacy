import { Component, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-eventful',
  template: `
    <p>
      Has thing been done? {{isThinged}}
      <button (click)="thing(true)">Do a thing</button>
      <button (click)="thing(false)">Undo a thing</button>
    </p>
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EventfulComponent {

  @Output() thinged = new EventEmitter<boolean>();
  isThinged: boolean = false;

  constructor() { }

  thing(thang: boolean) {
    this.thinged.emit(thang);
    this.isThinged = thang;
  }
}
