import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventfulComponent } from './eventful.component';

describe('EventfulComponent', () => {
  let component: EventfulComponent;
  let fixture: ComponentFixture<EventfulComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventfulComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventfulComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
