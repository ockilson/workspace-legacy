import { storiesOf } from '@storybook/angular';

import { StoryComponent } from './story.component';

const stories = storiesOf('StoryComponent', module);

stories.add('StoryComponent basic story', () => ({
        component: StoryComponent,
        notes: 'My notes on a basic story' 
    })
);