import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-story',
  template: `
    <p>
      This is a stupid component made as an example
    </p>
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StoryComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
