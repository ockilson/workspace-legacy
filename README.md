# Monorepo for some angular tooling

[![lerna](https://img.shields.io/badge/maintained%20with-lerna-cc00ff.svg)](https://lernajs.io/)

## Structure of angular app

```
root/
    - projects/ # libaries/secondary apps
    - src/ # main app source (default for angular)
    - e2e/ # main app e2e tests
    - packages/ # these are all npm packages
        - schematics-utils/
        - ng-jest/
        - local-schematics/
        - ng-storybook/
    - functions/ # ssr, prerender, audit etc using serverless functions [WIP]
    - tools/
        - .storybook/ # storybook configuration
        - schematics/
        - git-hooks/
    - .editorconfig
    - .gitignore
    - angular.json
    - jest.config.js
    - lerna.json
    - tsconfig.json
    - tslint.json
    - package.json
```

## Packages

* @ockilson/schematics-utils - see `./packages/schematics-utils/README.md`
* @ockilson/ng-jest - see `./packages/ng-jest/README.md`
* @ockilson/local-schematics - see `./packages/local-schematics/README.md`
* @ockilson/ng-storybook - see `./packages/ng-storybook/README.md`

Packages are built/tested/published using [lernajs](https://lernajs.io/) there are a few npm run scripts set up to handle these:

* `npm run pkg:test` - runs `npm run test` for every package in `packages/**/package.json`
* `npm run pkg:build` - runs `npm run build` for every package in `packages/**/package.json`
* `npm run pkg:runner <command>` - runs `npm run <command>` for every package in `packages/**/package.json`
* `npm run pkg:bootstrap` - runs `lerna bootstrap` to wire up dependency versions, symlink local directories etc.

Running schematics locally you need to link the package to the root project first.
* `cd packages/<package-name> && npm link`
* Return to the project root and run `npm link @ockilson/<package-name>`
* Run `ng g @ockilson/<package-name>:<command-name>`

### NB

When running locally before publshing a schematics package `ng add @ockilson/<package-name>` will fail, `ng g @ockilson/<package-name>:ng-add` does the same as `ng add @ockilson/<package-name>` and should work with the linked local packages.

## Angular Project

The root project is an angular app that uses the other packages, partially as an example but also will evolve into being my personal site as time allows.

### Local Schematics

This project uses the `@ockilson/local-schematics` package to generate additional package boilerplate. This is kept local as it is unlikely to be used anywhere else but also provides a good example usage of running schematics from within your project.

## CI/CD

### Node Packages

All branches with changes to `packages/**/*` will trigger:

* dependency installation
* build
* test

For all node packages. If *ANY* package build/tests fail the entire pipeline fails.

#### Publishing

Master will also run the lerna publish to bump the version and deploy all packages to npm as preminor, manual deployment of major/minor releases is still required as CI can't know how much impact changes have on end users.

## "Conventional Commits"

I am trying to force myself to use atomic commits by adopting the conventional commits style, to make sure all commits match expected format `commitlint` is ran prior to every commit and will reject any messages not matching the format below. You can skip over the checks using the `--no-verify` flag if you are particularly naughty.

```
<feat>(<scope?>): <subject of commit>

More detailed description of changes made

* Can have lists
* If you want
```

