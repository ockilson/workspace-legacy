# Add Jest to your @angular/cli apps

* `ng add @ockilson/ng-jest` - install and setup
* `ng g @ockilson/ng-jest:application` - create a new app using jest for tests
* `ng g @ockilson/ng-jest:library` - create a new library using jest for tests