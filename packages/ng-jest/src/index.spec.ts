import { Tree } from '@angular-devkit/schematics';
import { SchematicTestRunner, UnitTestTree } from '@angular-devkit/schematics/testing';
import { Schema as WorkspaceOptions } from '@schematics/angular/workspace/schema';
import { Schema as ApplicationOptions } from '@schematics/angular/application/schema';

const collectionPath = require.resolve('./collection.json');

describe('jest setup', () => {

    const testRunner = new SchematicTestRunner('ng-jest', collectionPath);

    const workspaceOptions: WorkspaceOptions = {
        name: 'workspace',
        newProjectRoot: 'projects',
        version: '7.0.5',
    };

    describe('new application', () => {
        const appOptions: ApplicationOptions = {
            name: 'bar',
            inlineStyle: false,
            inlineTemplate: false,
            routing: false,
            style: 'css',
            skipTests: false,
            skipPackageJson: false,
        };

        let appTree: UnitTestTree;
        beforeEach(() => {
            appTree = testRunner.runExternalSchematic('@schematics/angular', 'workspace', workspaceOptions);
        });

        it('fails with missing tree', () => {
            expect(() => testRunner.runSchematic('application', {
                name: "test"
            }, Tree.empty())).toThrow();
        });

        it('fails with missing params', () => {
            expect(() => testRunner.runSchematic('application', {}, appTree)).toThrowError();
        });

        it('works', () => {
            const tree = testRunner.runSchematic('application', appOptions, appTree);
            
            expect(tree.files).not.toContain("/projects/bar/src/app/test/test.ts");
            expect(tree.files).not.toContain("/projects/bar/karma.conf.js");
        });

    });

    describe('new library', () => {
        const appOptions: ApplicationOptions = {
            name: 'bar',
            inlineStyle: false,
            inlineTemplate: false,
            routing: false,
            style: 'css',
            skipTests: false,
            skipPackageJson: false,
        };

        let appTree: UnitTestTree;
        beforeEach(() => {
            appTree = testRunner.runExternalSchematic('@schematics/angular', 'workspace', workspaceOptions);
        });

        it('fails with missing tree', () => {
            expect(() => testRunner.runSchematic('library', {
                name: "test"
            }, Tree.empty())).toThrow();
        });

        it('fails with missing params', () => {
            expect(() => testRunner.runSchematic('library', {}, appTree)).toThrowError();
        });

        it('works', () => {
            const tree = testRunner.runSchematic('library', appOptions, appTree);
            
            expect(tree.files).not.toContain("/projects/bar/src/lib/test/test.ts");
            expect(tree.files).not.toContain("/projects/bar/karma.conf.js");
        });

    });
});