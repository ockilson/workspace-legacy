import { Rule, Tree, SchematicContext, chain, externalSchematic } from "@angular-devkit/schematics";
import { removeKarmaFilesFromProject } from "./karma";
import { addBuilderToAngularProjectConfig } from "./jest";

export function jestApplication(_options: any): Rule {
    return (tree: Tree, _context: SchematicContext) => {
        return chain([
            externalSchematic("@schematics/angular", "application", _options),
            removeKarmaFilesFromProject(_options),
            addBuilderToAngularProjectConfig(_options)
        ])
    }
}