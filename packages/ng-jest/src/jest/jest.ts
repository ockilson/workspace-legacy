import { Tree, SchematicContext, Rule, SchematicsException, branchAndMerge } from "@angular-devkit/schematics";
import { JsonAstObject, JsonAstArray } from "@angular-devkit/core";
import { readJsonFile, getProjectRootPath, getDefaultProject, getWorkspace, getProjectFromWorkspace, updateProjectInWorkspace, findPropertyInAstObject, appendPropertyInAstObject, appendValueInAstArray } from "@ockilson/schematics-utils";

const compilerOptions = {
    outDir: "../out-tsc/spec",
    module: "commonjs",
    types: [
        "jest"
    ]
}

export function addJestConfigToTsConfig(options: any, filePath: string): Rule {
    return (tree: Tree, _context: SchematicContext) => {
        options.project = options.project || getDefaultProject(tree);
        const projectPath = getProjectRootPath(tree, options.project);
        const tsSpecConfigPath = projectPath + filePath;
        const tsSpecConfigAst = readJsonFile(tree, tsSpecConfigPath);
        const compilerOptionsAst = findPropertyInAstObject(tsSpecConfigAst, 'compilerOptions') as JsonAstObject;
        const recorder = tree.beginUpdate(tsSpecConfigPath);

        if (!compilerOptionsAst) {
            appendPropertyInAstObject(
                recorder,
                tsSpecConfigAst,
                "compilerOptions",
                compilerOptions,
                2
            );
        } else {
            const typesArrayAst = findPropertyInAstObject(compilerOptionsAst, 'types');

            if (!typesArrayAst) {
                appendPropertyInAstObject(
                    recorder,
                    compilerOptionsAst,
                    "types",
                    compilerOptions.types,
                    4
                );
            } else {
                let jasmineHasComma:boolean = false;
                const jasmineTypeAst = (typesArrayAst as JsonAstArray).elements.filter((row) => {
                    let matches = (row.value as string) == 'jasmine';
                    if(matches) {
                        jasmineHasComma = false;
                    }
                    jasmineHasComma = true;
                    return matches;
                })[0];

                const jestTypeAst = (typesArrayAst as JsonAstArray).elements.filter((row) => {
                    return (row.value as string) == 'jest';
                })[0];

                if(jasmineTypeAst) {

                    const commaDangle = jasmineHasComma ? 1 : 0;
                    const end = jasmineTypeAst.end.offset + commaDangle;
                    const newLineIndentation = 7;

                    recorder.remove(
                        jasmineTypeAst.start.offset,
                        end - jasmineTypeAst.start.offset + newLineIndentation
                    );
                }

                if(!jestTypeAst) {
                    appendValueInAstArray(
                        recorder,
                        typesArrayAst as JsonAstArray,
                        "jest",
                        6
                    );
                }
            }

            if(options.compilerOptionsModule) {
                const modulesAst = findPropertyInAstObject(compilerOptionsAst, 'module');

                if(!modulesAst) {
                    appendPropertyInAstObject(
                        recorder,
                        compilerOptionsAst,
                        "module",
                        "commonjs",
                        4
                    );
                } else {
                    if(modulesAst.value !== "commonjs") {
                        recorder.remove(
                            modulesAst.start.offset,
                            modulesAst.end.offset + 5 - modulesAst.start.offset
                        );
                        appendPropertyInAstObject(
                            recorder,
                            compilerOptionsAst,
                            "module",
                            "commonjs",
                            4
                        );
                    }
                }

            }
        }

        tree.commitUpdate(recorder);
    }
}

export function addBuilderToAngularProjectConfig(options: any): Rule {
    return (tree: Tree, _context: SchematicContext) => {
        options.project = options.project || options.name || getDefaultProject(tree);

        const workspace = getWorkspace(tree);
        const project = getProjectFromWorkspace(workspace, options.project);

        if(!project.architect || !project.architect.test) {
            throw new SchematicsException(`No project architect configuration available for project ${options.project}`);
        }

        project.architect.test.builder = "@angular-builders/jest:run";

        // some default options are not compatible with jest so need to remove them
        const incompatibleProperties: string[] = [
            "main",
            "polyfills",
            "tsConfig",
            "karmaConfig"
        ];
        for(let prop of incompatibleProperties) {
            delete(project.architect.test.options[prop]);
        }

        return branchAndMerge(updateProjectInWorkspace(workspace, options.project, project));
    }
}