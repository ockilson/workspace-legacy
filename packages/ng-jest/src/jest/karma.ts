import { Rule, Tree, SchematicContext } from "@angular-devkit/schematics";
import { getProjectRootPath, getDefaultProject } from "@ockilson/schematics-utils";

export function removeKarmaFilesFromProject(options: any): Rule {
    return (tree: Tree, _context: SchematicContext) => {
        const projectName = options.project || options.name || getDefaultProject(tree);
        const projectPath = getProjectRootPath(tree, projectName);
        const karmaConfig = projectPath + '/karma.conf.js';

        if(tree.exists(karmaConfig)) {
            tree.delete(karmaConfig);
        }

        const testSetupPath = projectPath + '/src/test.ts';

        if(tree.exists(testSetupPath)) {
            tree.delete(testSetupPath);
        }

        return tree;
    }
}