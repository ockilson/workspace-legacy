import { Rule, Tree, SchematicContext, chain, externalSchematic } from "@angular-devkit/schematics";
import { removeKarmaFilesFromProject } from "./karma";
import { addBuilderToAngularProjectConfig } from "./jest";

export function jestLibrary(_options: any): Rule {
    return (tree: Tree, _context: SchematicContext) => {
        return chain([
            externalSchematic("@schematics/angular", "library", _options),
            removeKarmaFilesFromProject(_options),
            addBuilderToAngularProjectConfig(_options)
        ])
    }
}