import { chain, Tree, SchematicContext, Rule } from "@angular-devkit/schematics";
import { NodeDependencyType, removeDependenciesFromPackageJson, DeleteNodeDependency, NodeDependency, addDependenciesToPackageJson } from "@ockilson/schematics-utils";

import { removeKarmaFilesFromProject } from "./karma";
import { addJestConfigToTsConfig, addBuilderToAngularProjectConfig } from "./jest";

const dependencies: NodeDependency[] = [
    {
        type: NodeDependencyType.Dev,
        version: '^7.0.0',
        name: '@angular-builders/jest'
    },
    {
        type: NodeDependencyType.Dev,
        version: '^23.6.0',
        name: 'jest'
    },
    {
        type: NodeDependencyType.Dev,
        version: '^23.3.7',
        name: '@types/jest'
    }
];

const removeDeps: DeleteNodeDependency[] = [
    {
        name: "karma",
        type: NodeDependencyType.Dev
    },
    {
        name: "karma-chrome-launcher",
        type: NodeDependencyType.Dev
    },
    {
        name: "karma-coverage-istanbul-reporter",
        type: NodeDependencyType.Dev
    },
    {
        name: "karma-jasmine",
        type: NodeDependencyType.Dev
    },
    {
        name: "karma-jasmine-html-reporter",
        type: NodeDependencyType.Dev
    }
];

export function jestSetup(_options: any): Rule {
    return (tree: Tree, _context: SchematicContext) => {
        return chain([
            removeDependenciesFromPackageJson(_options, removeDeps),
            addDependenciesToPackageJson(_options, dependencies),
            removeKarmaFilesFromProject(_options),
            addJestConfigToTsConfig({
                ..._options,
                compilerOptionsModule: true
            }, '/src/tsconfig.spec.json'),
            addJestConfigToTsConfig(_options, '/tsconfig.json'),
            addBuilderToAngularProjectConfig(_options)
        ]);
    }
}