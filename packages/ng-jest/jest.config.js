module.exports = {
  preset: 'ts-jest',
  testMatch: ['**/+(*.)+(spec|test).+(ts|js)?(x)'],
  testEnvironment: 'node',
};