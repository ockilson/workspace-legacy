# Add Jest to your @angular/cli apps

* `ng add @ockilson/jest` - install and setup
* `ng g @ockilson/jest:application` - create a new app using jest for tests
* `ng g @ockilsn/jest:library` - create a new library using jest for tests