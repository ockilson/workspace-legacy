import { Tree } from '@angular-devkit/schematics';
import { SchematicTestRunner, UnitTestTree } from '@angular-devkit/schematics/testing';
import { Schema as WorkspaceOptions } from '@schematics/angular/workspace/schema';

const collectionPath = require.resolve('./collection.json');


describe('local-schematics', () => {

  const testRunner = new SchematicTestRunner('local-schematics', collectionPath);

  const workspaceOptions: WorkspaceOptions = {
    name: 'workspace',
    newProjectRoot: 'projects',
    version: '7.0.5',
  };

  let appTree: UnitTestTree;
  beforeEach(() => {
    appTree = testRunner.runExternalSchematic('@schematics/angular', 'workspace', workspaceOptions);
  });

  it('fails with missing tree', () => {
    expect(() => testRunner.runSchematic('local-schematics', {}, Tree.empty())).toThrow();
  });

  it('works', () => {
    const tree = testRunner.runSchematic('local-schematics', {}, appTree);

    expect(tree.files).toContain("/tsconfig.schematics.json");
    expect(tree.files).toContain("/schematics/src/collection.json");
  });
});
