import { Rule, SchematicContext, Tree, chain, SchematicsException } from '@angular-devkit/schematics';
import { addSchematicsFilesToProject, readJsonFile , findPropertyInAstObject, appendPropertyInAstObject , addScriptToPackageJson , addDependenciesToPackageJson, NodeDependency, NodeDependencyType, getWorkspace, getWorkspacePath } from '@ockilson/schematics-utils';

const dependencies: NodeDependency[] = [
  {
      type: NodeDependencyType.Dev,
      version: '^0.10.3',
      name: '@schematics/schematics'
  },
  {
      type: NodeDependencyType.Dev,
      version: '^7.0.3',
      name: '@schematics/angular'
  },
  {
      type: NodeDependencyType.Dev,
      version: '^7.0.3',
      name: '@angular-devkit/core'
  },
  {
      type: NodeDependencyType.Dev,
      version: '^7.0.3',
      name: '@angular-devkit/schematics'
  },
  {
    type: NodeDependencyType.Dev,
    version: 'latest',
    name: '@ockilson/schematics-utils'
  }
];


// You don't have to export the function as default. You can also have more than one rule factory
// per file.
export function localSchematics(_options: any): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    return chain([
      addSchematicsFilesToProject(_options, 'tools'),
      addSchematicsConfigToPackageJson(_options),
      addScriptToPackageJson(_options, "build:schematics", "tsc -p ./tools/schematics/tsconfig.json"),
      addDependenciesToPackageJson(_options, dependencies),
      addDefaultLocalSchematicToWorkspace(_options)
    ])
  };
}

function addDefaultLocalSchematicToWorkspace(options: any): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    const workspace = getWorkspace(tree);

    workspace.cli = {
      ...workspace.cli,
      defaultCollection: "."
    };

    tree.overwrite(getWorkspacePath(tree), JSON.stringify(workspace, null, 2));
    return tree;
  }
}

function addSchematicsConfigToPackageJson(options: any): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    const pkgJsonPath = "/package.json";
    const packageJsonAst = readJsonFile(tree, pkgJsonPath);
    const recorder = tree.beginUpdate(pkgJsonPath);

    const existing = findPropertyInAstObject(packageJsonAst, "schematics");
    if(existing) {
        throw new SchematicsException('Project package.json already includes schematics configuration.');
    }

    appendPropertyInAstObject(
        recorder,
        packageJsonAst,
        "schematics",
        "./schematics/src/collection.json",
        4
    );

    tree.commitUpdate(recorder);

    return tree;
  }
}