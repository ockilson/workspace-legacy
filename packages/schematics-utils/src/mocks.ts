import { UpdateRecorder } from "@angular-devkit/schematics";

export const recorderMock: UpdateRecorder = {
    insertLeft(index: number, content: Buffer | string): UpdateRecorder {
        return this;
    },
    insertRight(index: number, content: Buffer | string): UpdateRecorder {
        return this;
    },
    remove(index: number, length: number): UpdateRecorder {
        return this;
    }
}