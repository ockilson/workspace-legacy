import { Rule, Tree, SchematicContext, SchematicsException } from "@angular-devkit/schematics";
import { NodePackageInstallTask } from '@angular-devkit/schematics/tasks';
import { JsonAstObject, parseJsonAst, JsonParseMode } from "@angular-devkit/core";
import { findPropertyInAstObject, insertPropertyInAstObjectInOrder, appendPropertyInAstObject } from "./ast";
import { readJsonFile } from "./files";

const pkgJsonPath = '/package.json';

export enum NodeDependencyType {
    Default = 'dependencies',
    Dev = 'devDependencies',
    Peer = 'peerDependencies',
    Optional = 'optionalDependencies'
}

export interface NodeDependency {
    type: NodeDependencyType;
    name: string;
    version: string;
    overwrite?: boolean;
}

export interface DeleteNodeDependency {
    type: NodeDependencyType;
    name: string;
}

export function addPackageJsonDependency(tree: Tree, dependency: NodeDependency): void {
    const packageJsonAst = _readPackageJson(tree);
    const depsNode = findPropertyInAstObject(packageJsonAst, dependency.type);
    const recorder = tree.beginUpdate(pkgJsonPath);
    if (!depsNode) {
        // Haven't found the dependencies key, add it to the root of the package.json.
        appendPropertyInAstObject(
            recorder,
            packageJsonAst,
            dependency.type,
            {
                [dependency.name]: dependency.version
            },
            2
        );
    } else if (depsNode.kind === 'object') {
        // check if package already added
        const depNode = findPropertyInAstObject(depsNode, dependency.name);

        if (!depNode) {
            // Package not found, add it.
            insertPropertyInAstObjectInOrder(recorder, depsNode, dependency.name, dependency.version, 4);
        } else if (dependency.overwrite) {
            // Package found, update version if overwrite.
            const { end, start } = depNode;
            recorder.remove(start.offset, end.offset - start.offset);
            recorder.insertRight(start.offset, JSON.stringify(dependency.version));
        }
    }

    tree.commitUpdate(recorder);
}

export function getPackageJsonDependency(tree: Tree, name: string): NodeDependency | null {
    const packageJson = _readPackageJson(tree);
    let dep: NodeDependency | null = null;
    [NodeDependencyType.Default, NodeDependencyType.Dev, NodeDependencyType.Optional, NodeDependencyType.Peer].forEach(
        depType => {
            if (dep !== null) {
                return;
            }
            const depsNode = findPropertyInAstObject(packageJson, depType);
            if (depsNode !== null && depsNode.kind === 'object') {
                const depNode = findPropertyInAstObject(depsNode, name);
                if (depNode !== null && depNode.kind === 'string') {
                    const version = depNode.value;
                    dep = {
                        type: depType,
                        name: name,
                        version: version
                    };
                }
            }
        }
    );

    return dep;
}

export function removePackageJsonDependency(tree: Tree, dependency: DeleteNodeDependency): void {
    const packageJsonAst = _readPackageJson(tree);
    const depsNode = findPropertyInAstObject(packageJsonAst, dependency.type);
    const recorder = tree.beginUpdate(pkgJsonPath);

    if (!depsNode) {
        // Haven't found the dependencies key.
        new SchematicsException('Could not find the package.json dependency');
    } else if (depsNode.kind === 'object') {
        const fullPackageString = depsNode.text.split('\n').filter((pkg) => {
            return pkg.includes(`"${dependency.name}"`);
        })[0];

        const commaDangle =
            fullPackageString && fullPackageString.trim().slice(-1) === ',' ? 1 : 0;

        const packageAst = depsNode.properties.find((node) => {
            return node.key.value.toLowerCase() === dependency.name.toLowerCase();
        });

        // TODO: does this work for the last dependency?
        const newLineIndentation = 5;

        if (packageAst) {
            // Package found, remove it.
            const end = packageAst.end.offset + commaDangle;

            recorder.remove(
                packageAst.key.start.offset,
                end - packageAst.start.offset + newLineIndentation
            );
        }
    }

    tree.commitUpdate(recorder);
}

function _readPackageJson(tree: Tree): JsonAstObject {
    const buffer = tree.read(pkgJsonPath);
    if (buffer === null) {
        throw new SchematicsException('Could not read package.json.');
    }
    const content = buffer.toString();

    const packageJson = parseJsonAst(content, JsonParseMode.Strict);
    if (packageJson.kind != 'object') {
        throw new SchematicsException('Invalid package.json. Was expecting an object');
    }

    return packageJson;
}

export function addDependenciesToPackageJson(options: any, deps: NodeDependency[] = []): Rule {
    return (tree: Tree, context: SchematicContext) => {
        deps.map((dep) => addPackageJsonDependency(tree, dep));
        context.addTask(new NodePackageInstallTask());

        return tree;
    }
}

export function removeDependenciesFromPackageJson(options: any, deps: DeleteNodeDependency[] = []): Rule {
    return (tree: Tree, context: SchematicContext) => {
        deps.map((dep) => removePackageJsonDependency(tree, dep));
        context.addTask(new NodePackageInstallTask());

        return tree;
    }
}

export function addScriptToPackageJson(options: any, key: string, value: any): Rule {
    return (tree: Tree, context: SchematicContext) => {
        const packageJsonAst = readJsonFile(tree, pkgJsonPath);
        const recorder = tree.beginUpdate(pkgJsonPath);

        const existing = findPropertyInAstObject(packageJsonAst, "scripts");
        if(!existing) {
            let temp: any = {};
            temp[key] = value;
            appendPropertyInAstObject(
                recorder,
                packageJsonAst,
                "scripts",
                temp,
                2
            );
        } else {
            appendPropertyInAstObject(
                recorder,
                (existing as JsonAstObject),
                key,
                value,
                2
            );
        }

        tree.commitUpdate(recorder);

        return tree;
    }
}