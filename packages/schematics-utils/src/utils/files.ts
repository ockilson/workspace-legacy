import { JsonAstObject, JsonParseMode, parseJsonAst, strings } from "@angular-devkit/core";
import { Rule, SchematicContext, apply, url, move, template, mergeWith, branchAndMerge, chain, SchematicsException, Tree } from "@angular-devkit/schematics";

export function readJsonFile(tree: Tree, filePath: string): JsonAstObject {
    const buffer = tree.read(filePath);
    if (buffer === null) {
        throw new SchematicsException(`Could not read ${filePath}.`);
    }
    const content = buffer.toString();

    const packageJson = parseJsonAst(content, JsonParseMode.Strict);
    if (packageJson.kind != 'object') {
        throw new SchematicsException(`Invalid ${filePath}. Was expecting an object`);
    }

    return packageJson;
}

export function addSchematicsFilesToProject(options: any, dest: string = '', src: string = './files', modifiers: object = {}): Rule {
    return (tree: Tree, context: SchematicContext) => {
        let source = apply(url(src), [
            template({
                ...options as object,
                'if-flat': (s: string) => options.flat ? '' : s,
                camelize: strings.camelize,
                dasherize: strings.dasherize,
                classify: strings.classify,
                ...modifiers
            }),
            move(dest)
        ]);

        return chain([
            branchAndMerge(mergeWith(source))
        ]);
    }
}