import { UpdateRecorder } from "@angular-devkit/schematics";
import { recorderMock } from "../mocks";
import { JsonAstObject, parseJsonAst, JsonAstArray } from "@angular-devkit/core";
import { appendPropertyInAstObject, appendValueInAstArray } from "./json";

const _string = (ob: object) => JSON.stringify(ob, null, 2);

describe('appendPropertyInAstObject', () => {
    let rightSpy: any;
    let leftSpy: any;
    const recorder: UpdateRecorder = recorderMock;
    const existing = {
        "first": "thing"
    }
    const empty = {}

    beforeEach(() => {
        rightSpy = jest.spyOn(recorder, 'insertRight');
        leftSpy = jest.spyOn(recorder, 'insertLeft');
    })

    it('should add the new value and a comma to the right of the pointer if properties already exist', () => {
        const node: JsonAstObject = parseJsonAst(_string(existing)) as JsonAstObject;
        appendPropertyInAstObject(recorder, node, "second", "thing", 2);

        expect(rightSpy).toHaveBeenCalledWith(20, ",");
        expect(leftSpy).toHaveBeenCalledWith(21,
`  "second": "thing"
`
        );
    });

    it('should not add a comma to the right if properties dont already exist', () => {
        const node: JsonAstObject = parseJsonAst(_string(empty)) as JsonAstObject;
        appendPropertyInAstObject(recorder, node, "first", "thing", 2);

        expect(rightSpy).not.toHaveBeenCalled();
        expect(leftSpy).toHaveBeenCalledWith(1,
`  "first": "thing"
`
        );
    });

    afterEach(() => {
        rightSpy.mockRestore();
        leftSpy.mockRestore();
    });
    
});

describe('appendValueInAstArray', () => {
    let rightSpy: any;
    let leftSpy: any;
    const recorder: UpdateRecorder = recorderMock;
    const existing = [
        "first"
    ]
    const empty: any[] = []

    beforeEach(() => {
        rightSpy = jest.spyOn(recorder, 'insertRight');
        leftSpy = jest.spyOn(recorder, 'insertLeft');
    })

    it('should add the new value and a comma to the right of the pointer if properties already exist', () => {
        const node: JsonAstArray = parseJsonAst(_string(existing)) as JsonAstArray;
        appendValueInAstArray(recorder, node, "thing", 2);

        expect(rightSpy).toHaveBeenCalledWith(11, ",");
        expect(leftSpy).toHaveBeenCalledWith(12,
`  "thing"
`
        );
    });

    it('should not add a comma to the right if properties dont already exist', () => {
        const node: JsonAstArray = parseJsonAst(_string(empty)) as JsonAstArray;
        appendValueInAstArray(recorder, node, "thing", 2);

        expect(rightSpy).not.toHaveBeenCalled();
        expect(leftSpy).toHaveBeenCalledWith(1,
`  "thing"
`
        );
    });

    afterEach(() => {
        rightSpy.mockRestore();
        leftSpy.mockRestore();
    });
    
});