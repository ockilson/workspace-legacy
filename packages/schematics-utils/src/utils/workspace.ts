import { Tree } from "@angular-devkit/schematics/src/tree/interface";
import { SchematicsException, SchematicContext, Rule } from "@angular-devkit/schematics";
import { parseJson, JsonParseMode, experimental } from "@angular-devkit/core";

export type WorkspaceSchema = experimental.workspace.WorkspaceSchema;
export type WorkspaceProject = experimental.workspace.WorkspaceProject;

export function getWorkspacePath(host: Tree): string {
    const possibleFiles = ['/angular.json', '/.angular.json'];
    const path = possibleFiles.filter(path => host.exists(path))[0];

    return path;
}

export function getWorkspace(host: Tree): WorkspaceSchema {
    const path = getWorkspacePath(host);
    const configBuffer = host.read(path);
    if (configBuffer === null) {
        throw new SchematicsException(`Could not find (${path})`);
    }
    const content = configBuffer.toString();

    return (parseJson(content, JsonParseMode.Loose) as {}) as WorkspaceSchema;
}

export function addProjectToWorkspace(workspace: WorkspaceSchema, name: string, project: WorkspaceProject): Rule {
    return (host: Tree, context: SchematicContext) => {
        if (workspace.projects[name]) {
            throw new Error(`Project '${name}' already exists in workspace.`);
        }

        // Add project to workspace.
        workspace.projects[name] = project;

        if (!workspace.defaultProject && Object.keys(workspace.projects).length === 1) {
            // Make the new project the default one.
            workspace.defaultProject = name;
        }

        host.overwrite(getWorkspacePath(host), JSON.stringify(workspace, null, 2));
    };
}

export function getProjectFromWorkspace(workspace: WorkspaceSchema, name: string): WorkspaceProject {
    if (!workspace.projects[name]) {
        throw new Error(`Project '${name}' does not exist in workspace.`);
    }

    return workspace.projects[name];
}

export function updateProjectInWorkspace(workspace: WorkspaceSchema, name: string, project: WorkspaceProject): Rule {
    return (host: Tree, context: SchematicContext) => {
        if (!workspace.projects[name]) {
            throw new Error(`Project '${name}' does not exist in workspace.`);
        }

        workspace.projects[name] = project;

        host.overwrite(getWorkspacePath(host), JSON.stringify(workspace, null, 2));
    };
}

export function getProject(host: Tree, name: string): WorkspaceProject {
    const workspace: WorkspaceSchema = getWorkspace(host);
    return getProjectFromWorkspace(workspace, name);
}

export function getProjectRootPath(host: Tree, name: string): string {
    const project = getProject(host, name);

    if (project.root.substr(-1) === '/') {
        project.root = project.root.substr(0, project.root.length - 1);
    }

    return `${project.root ? `/${project.root}` : ''}`;
}

export function getProjectPath(host: Tree, name: string): string {
    const project = getProject(host, name);

    if (project.root.substr(-1) === '/') {
        project.root = project.root.substr(0, project.root.length - 1);
    }

    const projectDirName =
        project.projectType === 'application' ? 'app' : 'lib';

    return `${project.root ? `/${project.root}` : ''}/src/${projectDirName}`;
}

export function getDefaultProject(host: Tree): string {
    const workspace = getWorkspace(host);

    if(workspace.defaultProject) {
        return workspace.defaultProject;
    }

    if(Object.keys(workspace.projects).length < 1) {
        throw new SchematicsException('Workspace has no available projects');
    }

    return Object.keys(workspace.projects)[0];

}

export function isLib(host: Tree, name: string) {
    const project = getProject(host, name);

    return project.projectType === 'library';
}