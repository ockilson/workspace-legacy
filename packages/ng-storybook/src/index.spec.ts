import { Tree } from '@angular-devkit/schematics';
import { SchematicTestRunner, UnitTestTree } from '@angular-devkit/schematics/testing';
import { Schema as WorkspaceOptions } from '@schematics/angular/workspace/schema';
import { Schema as ApplicationOptions } from '@schematics/angular/application/schema';

const collectionPath = require.resolve('./collection.json');

describe('storybook setup', () => {

    const testRunner = new SchematicTestRunner('ng-storybook', collectionPath);

    const workspaceOptions: WorkspaceOptions = {
        name: 'workspace',
        newProjectRoot: 'projects',
        version: '7.0.5',
    };

    describe('ng-add command', () => {
        const appOptions: ApplicationOptions = {
            name: 'bar',
            inlineStyle: false,
            inlineTemplate: false,
            routing: false,
            style: 'css',
            skipTests: false,
            skipPackageJson: false,
        };

        let appTree: UnitTestTree;
        beforeEach(() => {
            appTree = testRunner.runExternalSchematic('@schematics/angular', 'workspace', workspaceOptions);
            appTree = testRunner.runExternalSchematic('@schematics/angular', 'application', appOptions, appTree);
        });

        it('fails with missing tree', () => {
            expect(() => testRunner.runSchematic('ng-add', {}, Tree.empty())).toThrow();
        });

        it('works', () => {
            const tree = testRunner.runSchematic('ng-add', appOptions, appTree);
            
            expect(tree.files).toContain("/.storybook/addons.js");
            expect(tree.files).toContain("/.storybook/config.js");
            expect(tree.files).toContain("/.storybook/tsconfig.json");
        });

    });

    describe('new component', () => {
        const appOptions: ApplicationOptions = {
            name: 'bar',
            inlineStyle: false,
            inlineTemplate: false,
            routing: false,
            style: 'css',
            skipTests: false,
            skipPackageJson: false,
        };

        let appTree: UnitTestTree;
        beforeEach(() => {
            appTree = testRunner.runExternalSchematic('@schematics/angular', 'workspace', workspaceOptions);
            appTree = testRunner.runExternalSchematic('@schematics/angular', 'application', appOptions, appTree);
        });

        it('fails with missing tree', () => {
            expect(() => testRunner.runSchematic('component', {
                name: "test"
            }, Tree.empty())).toThrow();
        });

        it('fails with missing params', () => {
            expect(() => testRunner.runSchematic('component', {}, appTree)).toThrowError();
        });

        it('works and creates new stories file', () => {
            const tree = testRunner.runSchematic('component', {
                name: "test",
                project: appOptions.name
            }, appTree);

            // NB: not testing the rest of the generated output inherited from @schematics/angular:component
            expect(tree.files).toContain("/projects/bar/src/app/test/test.component.stories.ts");

        });

    });
});