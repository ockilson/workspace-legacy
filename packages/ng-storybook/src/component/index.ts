import { Tree, chain, externalSchematic, Rule, SchematicContext } from '@angular-devkit/schematics';
import { Path, dirname, join, normalize } from '@angular-devkit/core';
import { addSchematicsFilesToProject, getProjectPath } from '@ockilson/schematics-utils';

export function storybookComponent(_options: any): Rule {
  return (tree: Tree, _context: SchematicContext) => {

    const componentPath = getProjectPath(tree, _options.project);
    const namePath = dirname(join(normalize(componentPath), _options.name) as Path);

    return chain([
      externalSchematic('@schematics/angular', 'component', {
        ..._options
      }),
      addSchematicsFilesToProject(_options, normalize('/' + namePath))
    ]);
  }
}
