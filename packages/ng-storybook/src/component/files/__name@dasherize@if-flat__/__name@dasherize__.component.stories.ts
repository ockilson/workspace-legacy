import { storiesOf } from '@storybook/angular';

import { <%= classify(name) %>Component } from './<%= dasherize(name) %>.component';

const stories = storiesOf('<%= classify(name) %>Component', module);

stories.add('<%= classify(name) %>Component basic story', () => ({
    component: <%= classify(name) %>Component
}));