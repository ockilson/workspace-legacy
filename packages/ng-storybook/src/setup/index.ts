import { chain, Tree, SchematicContext, Rule } from "@angular-devkit/schematics";
import { addSchematicsFilesToProject, addScriptToPackageJson , addDependenciesToPackageJson, NodeDependency, NodeDependencyType } from '@ockilson/schematics-utils';

const STORYBOOK_VERSION = '^4.0.4';

const dependencies: NodeDependency[] = [
    {
        type: NodeDependencyType.Dev,
        version: STORYBOOK_VERSION,
        name: '@storybook/angular'
    },
    {
        type: NodeDependencyType.Dev,
        version: STORYBOOK_VERSION,
        name: '@storybook/addon-links'
    },
    {
        type: NodeDependencyType.Dev,
        version: STORYBOOK_VERSION,
        name: '@storybook/addon-notes'
    },
    {
        type: NodeDependencyType.Dev,
        version: STORYBOOK_VERSION,
        name: '@storybook/addon-a11y'
    },
    {
        type: NodeDependencyType.Dev,
        version: STORYBOOK_VERSION,
        name: '@storybook/actions'
    },
    {
        type: NodeDependencyType.Dev,
        version: '^7.1.5',
        name: '@babel/core'
    },
    {
        type: NodeDependencyType.Dev,
        version: '^8.0.4',
        name: 'babel-loader'
    }
];

export function setupStorybook(_options: any): Rule {
    return (tree: Tree, _context: SchematicContext) => {
        return chain([
            addSchematicsFilesToProject(_options, 'tools'),
            addScriptToPackageJson(_options, "storybook", "start-storybook -p 9001 -c ./tools/.storybook"),
            addDependenciesToPackageJson(_options, dependencies)
        ]);
    }
}