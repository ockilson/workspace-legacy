# Add storybookjs to your @angular/cli apps

* `ng add @ockilson/ng-storybook` - install and setup
* `ng g @ockilson/ng-storybook:component` - create a new component with story file
* `npm run storybook` - run storybook, open browser to localhost:9001 to view ui